import { Response } from "express";
import { User } from "../entity/User";
import { sign } from "jsonwebtoken";

export const sendRefreshToken = (res: Response, token: string) => {
  res.cookie("j", token, {
    httpOnly: true,
    //path: "/_rt",
    expires: new Date(Date.now() + 60 * 60 * 24 * 7 * 1000),
  });
};

export const createAccessToken = (user: User) => {
  return sign({ userId: user.id }, process.env.ACCESS_TOKEN_SECRET!, {
    expiresIn: "15m",
  });
};

export const createRefreshToken = (user: User) => {
  return sign(
    { userId: user.id, tokenVersion: user.tokenVersion },
    process.env.REFRESH_TOKEN_SECRET!,
    {
      expiresIn: "7d",
    },
  );
};

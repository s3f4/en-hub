import { DictionaryType } from "./../entity/DictionaryType";
import {
  Resolver,
  Mutation,
  Arg,
  InputType,
  Field,
  Query,
  ID,
  Int,
  FieldResolver,
  Root,
} from "type-graphql";
import { Dictionary } from "../entity/Dictionary";
import { Length } from "class-validator";
import convertURL from "../lib/url";
@InputType()
export class DictionaryInput {
  @Field(() => ID, { nullable: true })
  id: number;

  @Field({ nullable: true })
  @Length(10, 100)
  title: string;

  @Field({ nullable: true })
  @Length(10, 3000)
  content: string;

  @Field({ nullable: true })
  dictionaryType: number;

  @Field({ nullable: true })
  @Length(10, 100)
  slug: string;
}

@Resolver(() => Dictionary)
export class DictionaryResolver {
  @Mutation(() => Dictionary, { nullable: true })
  async addDictionary(
    @Arg("dictionaryInput") dictionaryInput: DictionaryInput,
  ) {
    try {
      const dictionary = new Dictionary();
      dictionary.content = dictionaryInput.content;
      dictionary.title = dictionaryInput.title;
      dictionary.slug = convertURL(dictionaryInput.title);

      const dictionaryType:
        | DictionaryType
        | undefined = await DictionaryType.findOne(
        1, //dictionaryInput.dictionaryType,
      );
      if (dictionaryType) {
        dictionary.dictionaryType = dictionaryType.id;
      } else {
        return null;
      }

      return await dictionary.save();
    } catch (error) {
      console.log(error);
      return null;
    }
  }

  @Mutation(() => Dictionary, { nullable: true })
  async updateDictionary(
    @Arg("dictionaryInput") dictionaryInput: DictionaryInput,
  ) {
    try {
      const dictionary = await Dictionary.findOne({
        where: { slug: dictionaryInput.slug },
      });

      if (dictionary) {
        dictionary.title = dictionaryInput.title;
        dictionary.content = dictionaryInput.content;
        return await dictionary.save();
      } else {
        return null;
      }
    } catch (error) {
      console.log(error);
      return null;
    }
  }

  @Mutation(() => Boolean)
  async deleteDictionary(
    @Arg("dictionaryInput") dictionaryInput: DictionaryInput,
  ) {
    try {
      await Dictionary.delete({
        slug: dictionaryInput.slug,
      });

      return true;
    } catch (error) {
      console.log(error);
      return false;
    }
  }

  @Query(() => Dictionary, { nullable: true })
  async getDictionary(@Arg("slug") slug: string) {
    return await Dictionary.findOne({
      where: {
        slug,
      },
    });
  }

  @Query(() => [Dictionary])
  async listDictionaries(
    @Arg("limit", () => Int) limit: number,
    @Arg("offset", () => Int) offset: number,
    @Arg("dictionaryType", () => String, { nullable: true })
    dictionaryType: string,
  ) {
    console.log(dictionaryType);
    if (!dictionaryType) {
      return await Dictionary.find({
        skip: offset,
        take: limit,
      });
    } else {
      const dt = await DictionaryType.findOne({ slug: dictionaryType });
      if (dt) {
        return await Dictionary.find({
          where: {
            dictionaryType: {
              id: dt.id,
            },
          },
          skip: offset,
          take: limit,
        });
      }
    }
    return null;
  }
  @FieldResolver()
  async dictionaryType(@Root() dictionary: Dictionary) {
    return await DictionaryType.findOne(dictionary.dictionaryType);
  }

  @Query(() => [DictionaryType])
  async listDictionaryTypes() {
    return await DictionaryType.find({});
  }
}

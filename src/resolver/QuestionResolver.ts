import {
  Resolver,
  Mutation,
  Arg,
  InputType,
  Field,
  Query,
  ID,
  Int,
  FieldResolver,
  Root,
} from "type-graphql";
import { MaxLength, Length } from "class-validator";
import convertURL from "../lib/url";
import { Question } from "../entity/Question";
import { Answer } from "./../entity/Answer";
@InputType()
export class QuestionInput {
  @Field(() => ID, { nullable: true })
  id: number;

  @Field({ nullable: true })
  @MaxLength(100)
  title: string;

  @Field({ nullable: true })
  @Length(10, 3000)
  content: string;

  @Field({ nullable: true })
  slug: string;
}

@Resolver(() => Question)
export class QuestionResolver {
  @Mutation(() => Question, { nullable: true })
  async addQuestion(@Arg("questionInput") questionInput: QuestionInput) {
    try {
      const question = new Question();
      question.content = questionInput.content;
      question.title = questionInput.title;
      question.slug = convertURL(question.title);
      question.userId = 1;
      return await question.save();
    } catch (error) {
      console.log(error);
      return null;
    }
  }

  @Mutation(() => Question, { nullable: true })
  async updateQuestion(@Arg("questionInput") questionInput: QuestionInput) {
    try {
      const question = await Question.findOne({
        where: { slug: questionInput.slug },
      });

      if (question) {
        question.title = questionInput.title;
        question.content = questionInput.content;
        return await question.save();
      } else {
        return null;
      }
    } catch (error) {
      console.log(error);
      return null;
    }
  }

  @Mutation(() => Boolean)
  async deleteQuestion(@Arg("questionInput") questionInput: QuestionInput) {
    try {
      await Question.delete({
        slug: questionInput.slug,
      });
      return true;
    } catch (error) {
      return false;
    }
  }

  @Query(() => Question, { nullable: true })
  async getQuestion(@Arg("slug") slug: string) {
    try {
      return await Question.findOne({
        where: {
          slug,
        },
      });
    } catch (error) {
      console.log(error);
      return null;
    }
  }

  @Query(() => [Question], { nullable: true })
  async listQuestions(
    @Arg("limit", () => Int) limit: number,
    @Arg("offset", () => Int) offset: number,
  ) {
    try {
      return await Question.find({
        skip: offset,
        take: limit,
      });
    } catch (error) {
      console.log(error);
      return null;
    }
  }

  @FieldResolver(() => [Answer], { nullable: true })
  async answers(@Root() question: Question) {
    try {
      return await Answer.find({
        questionId: question.id,
      });
    } catch (error) {
      console.log(error);
      return null;
    }
  }
}

import {
  Resolver,
  Mutation,
  Arg,
  InputType,
  Field,
  Query,
  ID,
  Int,
} from "type-graphql";
import { Topic } from "../entity/Topic";
import { MaxLength, Length } from "class-validator";
import convertURL from "../lib/url";
@InputType()
export class TopicInput {
  @Field(() => ID, { nullable: true })
  id: number;

  @Field({ nullable: true })
  @MaxLength(100)
  title: string;

  @Field({ nullable: true })
  @Length(10, 3000)
  content: string;

  @Field({ nullable: true })
  slug: string;

  @Field({ nullable: true })
  topicIndex: number;
}

@Resolver()
export class TopicResolver {
  @Mutation(() => Topic, { nullable: true })
  async addTopic(@Arg("topicInput") topicInput: TopicInput) {
    try {
      const topic = new Topic();
      topic.content = topicInput.content;
      topic.title = topicInput.title;
      topic.slug = convertURL(topic.title);
      return await topic.save();
    } catch (error) {
      console.log(error);
      return null;
    }
  }

  @Mutation(() => Topic, { nullable: true })
  async updateTopic(@Arg("topicInput") topicInput: TopicInput) {
    try {
      const topic = await Topic.findOne({ where: { slug: topicInput.slug } });

      if (topic) {
        topic.title = topicInput.title;
        topic.content = topicInput.content;
        return await topic.save();
      } else {
        return null;
      }
    } catch (error) {
      console.log(error);
      return null;
    }
  }

  @Mutation(() => Boolean)
  async deleteTopic(@Arg("topicInput") topicInput: TopicInput) {
    try {
      await Topic.delete({
        slug: topicInput.slug,
      });
      return true;
    } catch (error) {
      return false;
    }
  }

  @Query(() => Topic, { nullable: true })
  async getTopic(@Arg("slug") slug: string) {
    return Topic.findOne({
      where: {
        slug,
      },
    });
  }

  @Query(() => [Topic], { nullable: true })
  async listTopics(
    @Arg("limit", () => Int) limit: number,
    @Arg("offset", () => Int) offset: number,
  ) {
    return Topic.find({
      skip: offset,
      take: limit,
    });
  }
}

import { isAuth } from "./../middleware/auth";
import {
  Resolver,
  Mutation,
  Query,
  Ctx,
  Arg,
  Subscription,
  Root,
  PubSub,
  Publisher,
  Field,
  ObjectType,
  UseMiddleware,
} from "type-graphql";
import { User } from "../entity/User";
import { Context } from "../lib/context";
import { hash, compare } from "bcryptjs";
import {
  sendRefreshToken,
  createRefreshToken,
  createAccessToken,
} from "../auth/token";

@ObjectType()
class AuthResponse {
  @Field()
  accessToken: string;
  @Field(() => User)
  user: User;
}

@Resolver()
export class UserResover {
  @Query(() => User, { nullable: true })
  @UseMiddleware(isAuth)
  async profile(@Ctx() context: Context) {
    if (context.payload && context.payload.userId) {
      const user = await User.findOne(context.payload.userId);
      return user;
    }

    return null;
  }

  @Mutation(() => Boolean)
  @UseMiddleware(isAuth)
  async logOut(@Ctx() ctx: Context) {
    sendRefreshToken(ctx.res, "");
    return true;
  }

  @Mutation(() => AuthResponse)
  async signUp(
    @PubSub("SIGNUP") publish: Publisher<User>,
    @Arg("email") email: string,
    @Arg("password") password: string,
    @Ctx() ctx: Context,
  ) {
    const user = await User.findOne({ where: { email } });

    if (user) {
      throw new Error("this email is taken already...");
    }

    const hashedPassword = await hash(password, 10);

    try {
      const newUser = new User();
      newUser.email = email;
      newUser.password = hashedPassword;
      await publish(newUser);
      await newUser.save();

      sendRefreshToken(ctx.res, createRefreshToken(newUser));

      return { accessToken: createAccessToken(newUser), newUser };
    } catch (error) {
      return null;
    }
  }

  @Mutation(() => AuthResponse)
  async signIn(
    @Arg("email") email: string,
    @Arg("password") password: string,
    @Ctx() { res }: Context,
  ) {
    const user = await User.findOne({ where: { email } });

    if (!user) {
      throw new Error("user not found");
    }

    const validPassword = await compare(password, user.password);

    if (!validPassword) {
      throw new Error("password is not correct");
    }

    sendRefreshToken(res, createRefreshToken(user));
    return { accessToken: createAccessToken(user), user };
  }

  @Subscription({
    topics: "SIGNUP",
  })
  user(@Root() user: User): User {
    return user;
  }
}

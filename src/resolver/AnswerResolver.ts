import { Question } from "./../entity/Question";
import {
  Resolver,
  Mutation,
  Arg,
  InputType,
  Field,
  Query,
  ID,
  FieldResolver,
  Root,
} from "type-graphql";
import { Answer } from "../entity/Answer";
import { MaxLength } from "class-validator";
@InputType()
export class AnswerInput {
  @Field(() => ID, { nullable: true })
  id: number;

  @Field(() => ID, { nullable: true })
  questionId: number;

  @Field({ nullable: true })
  @MaxLength(1000)
  answer: string;
}

@Resolver(() => Answer)
export class AnswerResolver {
  @Mutation(() => Answer, { nullable: true })
  async addAnswer(@Arg("answerInput") answerInput: AnswerInput) {
    try {
      const answer = new Answer();
      answer.answer = answerInput.answer;
      answer.questionId = answerInput.questionId;
      return await answer.save();
    } catch (error) {
      console.log(error);
      return null;
    }
  }

  @Mutation(() => Answer, { nullable: true })
  async updateAnswer(@Arg("answerInput") answerInput: AnswerInput) {
    try {
      const answer = await Answer.update(answerInput.id, {
        answer: answerInput.answer,
      });

      return answer;
    } catch (error) {
      console.log(error);
      return null;
    }
  }

  @Mutation(() => Boolean)
  async deleteAnswer(@Arg("id") id: number) {
    return await Answer.delete(id);
  }

  @Query(() => [Answer])
  async listAnswers() {
    return await Answer.find({});
  }

  @FieldResolver(() => Question, { nullable: true })
  async question(@Root() answer: Answer) {
    try {
      return await Question.findOne(answer.questionId);
    } catch (error) {
      return null;
    }
  }
}

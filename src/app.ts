import { TopicResolver } from "./resolver/TopicResolver";
import http from "http";
import express, { Application } from "express";
import "reflect-metadata";
import "dotenv/config";
import cors from "cors";
import cookieParser from "cookie-parser";
import { verify } from "jsonwebtoken";
import { User } from "./entity/User";
import { Payload } from "./lib/payload";
import { ApolloServer, PubSub, Config } from "apollo-server-express";
import { buildSchema } from "type-graphql";
import {
  sendRefreshToken,
  createRefreshToken,
  createAccessToken,
} from "./auth/token";
import { createConnection } from "typeorm";
import { UserResover } from "./resolver/UserResolver";
import { Context } from "./lib/context";
import { QuestionResolver } from "./resolver/QuestionResolver";
import { DictionaryResolver } from "./resolver/DictionaryResolver";
import { AnswerResolver } from "./resolver/AnswerResolver";

const myPlugin = {
  // Fires whenever a GraphQL request is received from a client.
  requestDidStart(requestContext: any) {
    console.log("Request started! Query:\n" + requestContext.request.query);
    // console.log(requestContext);
    return {
      // Fires whenever Apollo Server will parse a GraphQL
      // request to create its associated document AST.
      parsingDidStart(requestContext: any) {
        requestContext!;
        // console.log(requestContext);
        // console.log("Parsing started!");
      },

      // Fires whenever Apollo Server will validate a
      // request's document AST against your GraphQL schema.
      validationDidStart(requestContext: any) {
        requestContext!;
        // console.log(requestContext);
        // console.log("Validation started!");
      },
    };
  },
};

(async () => {
  const app: Application = express();
  app.use(cors({ origin: "http://localhost:3000", credentials: true }));
  app.use(cookieParser());

  app.post("/_rt", async (req, res) => {
    const token = req.cookies.j;

    if (!token) {
      return res.send({ ok: false, accessToken: "" });
    }

    let payload: Payload;
    try {
      payload = verify(token, process.env.REFRESH_TOKEN_SECRET!) as any;
    } catch (error) {
      console.log(error);
      return res.send({ ok: false, accessToken: "" });
    }

    const user = await User.findOne(payload.userId);

    if (!user) {
      return res.send({ ok: false, accessToken: "" });
    }

    if (user.tokenVersion !== payload.tokenVersion) {
      return res.send({ ok: false, accessToken: "" });
    }

    sendRefreshToken(res, createRefreshToken(user));

    return res.send({ ok: true, accessToken: createAccessToken(user) });
  });

  await createConnection();

  const PORT = 4000;
  const pubsub = new PubSub();
  const serverConfig: Config = {
    schema: await buildSchema({
      resolvers: [
        UserResover,
        TopicResolver,
        QuestionResolver,
        AnswerResolver,
        DictionaryResolver,
      ],
    }),
    context: ({ req, res }) => ({ req, res, pubsub } as Context),
    engine: {
      debugPrintReports: true,
    },
    plugins: [myPlugin],
  };

  const apolloServer = new ApolloServer(serverConfig);
  apolloServer.applyMiddleware({ app, cors: false });

  const httpServer = http.createServer(app);
  apolloServer.installSubscriptionHandlers(httpServer);

  httpServer.listen(4000, () => {
    console.log(
      `🚀 Server ready at http://localhost:${PORT}${apolloServer.graphqlPath}`,
    );
    console.log(
      `🚀 Subscriptions ready at ws://localhost:${PORT}${apolloServer.subscriptionsPath}`,
    );
  });
})();

import {
  CreateDateColumn,
  UpdateDateColumn,
  BaseEntity as Base,
} from "typeorm";
import { Field } from "type-graphql";

export class BaseEntity extends Base {
  @CreateDateColumn({
    type: "timestamp",
    default: () => "CURRENT_TIMESTAMP(6)",
  })
  @Field()
  createdAt: Date;

  @UpdateDateColumn({
    type: "timestamp",
    default: () => "CURRENT_TIMESTAMP(6)",
    onUpdate: "CURRENT_TIMESTAMP(6)",
  })
  @Field()
  updatedAt: Date;
}

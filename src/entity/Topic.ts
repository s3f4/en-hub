import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { ObjectType, Field, Int } from "type-graphql";
import { BaseEntity } from "./BaseEntity";

@ObjectType()
@Entity()
export class Topic extends BaseEntity {
  @Field(() => Int)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column("int", { nullable: true })
  topicIndex: number;

  @Field()
  @Column("text")
  title: string;

  @Field()
  @Column("text")
  content: string;

  @Field()
  @Column("text")
  slug: string;

  @Field()
  createdAt: Date;

  @Field()
  updatedAt: Date;
}

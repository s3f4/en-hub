import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { ObjectType, Field, Int } from "type-graphql";
import { BaseEntity } from "./BaseEntity";

@ObjectType()
@Entity()
export class Answer extends BaseEntity {
  @Field(() => Int)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column("int")
  questionId: number;

  @Field(() => Int)
  @Column("int")
  userId: number;

  @Field()
  @Column("text")
  answer: string;

  @Field()
  createdAt: Date;

  @Field()
  updatedAt: Date;
}

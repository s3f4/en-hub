import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { ObjectType, Field, Int } from "type-graphql";
import { BaseEntity } from "./BaseEntity";

@ObjectType()
@Entity()
export class Question extends BaseEntity {
  @Field(() => Int)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column("text")
  title: string;

  @Field()
  @Column("text")
  content: string;

  @Field(() => Int)
  @Column("int")
  userId: number;

  @Field(() => Int)
  questionType: number;

  @Field()
  @Column("text")
  slug: string;

  @Field()
  createdAt: Date;

  @Field()
  updatedAt: Date;
}

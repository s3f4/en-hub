import { Dictionary } from "./Dictionary";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BaseEntity,
  OneToMany,
} from "typeorm";
import { ObjectType, Field, Int } from "type-graphql";

@ObjectType()
@Entity()
export class DictionaryType extends BaseEntity {
  @Field(() => Int)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column("text")
  slug: string;

  @Field()
  @Column("text")
  name: string;

  @OneToMany(
    () => Dictionary,
    dictionary => dictionary.dictionaryType,
  )
  list: Dictionary[];
}

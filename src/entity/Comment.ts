import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { ObjectType, Field, Int } from "type-graphql";
import { BaseEntity } from "./BaseEntity";

@ObjectType()
@Entity()
export class Comment extends BaseEntity {
  @Field(() => Int)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column("text")
  comment: string;

  @Field()
  @Column("text")
  contentId: number;

  @Column("int", { default: 0 })
  dictionaryType: number;

  @Field()
  createdAt: Date;

  @Field()
  updatedAt: Date;
}

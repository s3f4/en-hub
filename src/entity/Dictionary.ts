import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { ObjectType, Field, Int } from "type-graphql";
import { DictionaryType } from "./DictionaryType";
import { BaseEntity } from "./BaseEntity";

@ObjectType()
@Entity()
export class Dictionary extends BaseEntity {
  @Field(() => Int)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column("text")
  title: string;

  @Field()
  @Column("text")
  content: string;

  @Field(() => DictionaryType)
  @ManyToOne(
    () => DictionaryType,
    dictionaryType => dictionaryType.list,
  )
  dictionaryType: number;

  @Field()
  @Column("text")
  slug: string;

  @Field()
  createdAt: Date;

  @Field()
  updatedAt: Date;
}

import { Response, Request } from "express";
import { Payload } from "./payload";
import { PubSub } from "apollo-server-express";

export interface Context {
  res: Response;
  req: Request;
  payload?: Payload;
  pubsub: PubSub;
}

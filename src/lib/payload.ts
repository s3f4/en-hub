export interface Payload {
  userId: string;
  tokenVersion: number;
}

import slugify from "slugify";
const convertURL = (content: string) =>
  slugify(content, {
    replacement: "-",
    remove: /[*+~.()'"!:@]/g,
    lower: true,
  } as any);

export default convertURL;
